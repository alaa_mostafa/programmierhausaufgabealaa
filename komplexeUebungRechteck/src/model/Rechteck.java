package model;

import java.util.Random;

import model.Punkt;

public class Rechteck {
	private Punkt p = new Punkt();
	private int hoehe;
	private int breite;

	public Rechteck() {
		this.p.setX(0);
		this.p.setY(0);
		this.hoehe = 0;
		this.breite = 0;

	}

	public Rechteck(int PosX, int PosY, int hoehe, int breite) {
		this.p.getX();
		this.p.getY();
		this.hoehe = hoehe;
		this.breite = breite;
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		this.hoehe = hoehe;
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		this.breite = breite;
	}

	@Override
	public String toString() {
		return "Rechteck [p=" + p + ", hoehe=" + hoehe + ", breite=" + breite + "]";
	}

	public Punkt getP() {
		return p;
	}

	public void setP(Punkt p) {
		this.p = p;
	}

	public boolean enthaelt(int x, int y) {

		return breite > 0 && hoehe > 0 && x >= this.p.getX() && x < this.p.getX() + breite && y >= this.p.getY()
				&& y < this.p.getY() + hoehe;
	}

	public boolean enthaelt(Punkt p) {

		return enthaelt(p.getX(), p.getY());

	}

	public boolean enthaelt(Rechteck rechteck) {
		return breite > 0 && hoehe > 0 && rechteck.getBreite() > 0 && rechteck.getHoehe() > 0
				&& rechteck.getP().getX() >= this.p.getX()
				&& rechteck.getP().getX() + rechteck.getBreite() <= this.p.getX() + this.breite
				&& rechteck.getP().getY() >= this.p.getY()
				&& rechteck.getP().getY() + rechteck.getHoehe() <= this.p.getY() + this.hoehe;
	}

	public static Rechteck generiereZufallsRechteck() {
		Random r = new Random();
		int x = r.nextInt(1200);
		int y = r.nextInt(1000);
		int new_breite = r.nextInt(x);
		int new_hoehe = r.nextInt(y);

		return new Rechteck(x, y, new_breite, new_hoehe);

	}

}
