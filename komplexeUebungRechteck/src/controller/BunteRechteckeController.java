package controller;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

import model.Punkt;
import model.Rechteck;

public class BunteRechteckeController {
	private LinkedList<Rechteck> rechtecke = new LinkedList<Rechteck>();
	private Rechteck r = new Rechteck();

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public BunteRechteckeController() {
		rechtecke = new LinkedList<Rechteck>();

	}

	public void add(Rechteck rechteck) {

		rechtecke.add(rechteck);

	}

	public void reset() {
		rechtecke.clear();
	}

	public LinkedList<Rechteck> getRechtecke() {
		return rechtecke;
	}

	@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}

	 public
	 void generiereZufallsRechtecke(int anzahl) {
		 reset();
		 for (int i = 0; i < anzahl; i++) {
			Random re = new Random();
			int x = re.nextInt(1200);
			int y = re.nextInt(1200);
			r.setP(new Punkt(x,y));
            r.setBreite(re.nextInt(x));
            r.setHoehe(re.nextInt(y));
            add(r);
			
			
		}
	 }
	
	//test1
}
