package test;
import model.Punkt;
import model.Rechteck;
import controller.BunteRechteckeController;
public class RechteckTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Rechteck.java
		Rechteck rechteck0 = new Rechteck(10,10,30,40);
		Rechteck rechteck1 = new Rechteck(25,25,100,20);
		Rechteck rechteck2 = new Rechteck(260,10,200,100);
		Rechteck rechteck3 = new Rechteck(5,500,300,25);
		Rechteck rechteck4 = new Rechteck(100,100,100,100);
		
		Rechteck rechteck5 = new Rechteck();
		rechteck5.setP(new Punkt(200, 200));
		rechteck5.setHoehe(200);
		rechteck5.setBreite(200);
		
		Rechteck rechteck6 = new Rechteck();
		rechteck6.setP(new Punkt(800, 400));
		rechteck6.setHoehe(20);
		rechteck6.setBreite(20);
		
		Rechteck rechteck7 = new Rechteck();
		rechteck7.setP(new Punkt(800, 450));
		rechteck7.setHoehe(20);
		rechteck7.setBreite(20);
		
		Rechteck rechteck8 = new Rechteck();
		rechteck8.setP(new Punkt(850, 400));
		rechteck8.setHoehe(20);
		rechteck8.setBreite(20);
		
		Rechteck rechteck9 = new Rechteck();
		rechteck9.setP(new Punkt(855, 455));
		rechteck9.setHoehe(25);
		rechteck9.setBreite(25);
		
		
		System.out.println("Rechteck 0 " + "Position X: " + rechteck0.getP().getX() + "Position Y: " + rechteck0.getP().getY() + "H�he: " + rechteck0.getHoehe() + "Breite: " + rechteck0.getBreite());
		System.out.println("Rechteck 1 " + "Position X: " + rechteck1.getP().getX() + "Position Y: " + rechteck1.getP().getY() + "H�he: " + rechteck1.getHoehe() + "Breite: " + rechteck1.getBreite());
		System.out.println("Rechteck 2 " + "Position X: " + rechteck2.getP().getX() + "Position Y: " + rechteck2.getP().getY() + "H�he: " + rechteck2.getHoehe() + "Breite: " + rechteck2.getBreite());
		System.out.println("Rechteck 3 " + "Position X: " + rechteck3.getP().getX() + "Position Y: " + rechteck3.getP().getY() + "H�he: " + rechteck3.getHoehe() + "Breite: " + rechteck3.getBreite());
		System.out.println("Rechteck 4 " + "Position X: " + rechteck4.getP().getX() + "Position Y: " + rechteck4.getP().getY() + "H�he: " + rechteck4.getHoehe() + "Breite: " + rechteck4.getBreite());
		System.out.println("Rechteck 5 " + "Position X: " + rechteck5.getP().getX() + "Position Y: " + rechteck5.getP().getY() + "H�he: " + rechteck5.getHoehe() + "Breite: " + rechteck5.getBreite());
		System.out.println("Rechteck 6 " + "Position X: " + rechteck6.getP().getX() + "Position Y: " + rechteck6.getP().getY() + "H�he: " + rechteck6.getHoehe() + "Breite: " + rechteck6.getBreite());
		System.out.println("Rechteck 7 " + "Position X: " + rechteck7.getP().getX()+ "Position Y: " + rechteck7.getP().getY() + "H�he: " + rechteck7.getHoehe() + "Breite: " + rechteck7.getBreite());
		System.out.println("Rechteck 8 " + "Position X: " + rechteck8.getP().getX() + "Position Y: " + rechteck8.getP().getY() + "H�he: " + rechteck8.getHoehe() + "Breite: " + rechteck8.getBreite());
		System.out.println("Rechteck 9 " + "Position X: " + rechteck9.getP().getX() + "Position Y: " + rechteck9.getP().getY() + "H�he: " + rechteck9.getHoehe() + "Breite: " + rechteck9.getBreite());
		System.out.println("");
		System.out.println("");
		System.out.println("---- k�rzere Variante ----");
		System.out.println("");
		System.out.println(rechteck0.toString());
		//System.out.println(rechteck0.toString().equals("Rechteck [PosX=10, PosY=10, hoehe=30, breite=40]")); // equals vergleicht
		// ich habe es auf meine Atribute angepasst
		System.out.println(rechteck0.toString().equals("Rechteck 0: [x=10, y=10, breite=30, hoehe=40]"));
		
		
		//BunteRechteckeController
		BunteRechteckeController controller = new BunteRechteckeController();
		controller.add(rechteck0);
		controller.add(rechteck1);
		controller.add(rechteck2);
		controller.add(rechteck3);
		controller.add(rechteck4);
		controller.add(rechteck5);
		controller.add(rechteck6);
		controller.add(rechteck7);
		controller.add(rechteck8);
		controller.add(rechteck9);
		
		System.out.println(controller.toString());
	}
	
	public static boolean rechteckeTesten() {
		
		  Rechteck flaeche = new Rechteck(0, 0, 1200, 1000);
	        Rechteck[] rechtecke = new Rechteck[50000];
	        for (int i = 0; i < rechtecke.length; i++) {
	            rechtecke[i] = new Rechteck();
	            rechtecke[i].generiereZufallsRechteck();
	            if (flaeche.enthaelt(rechtecke[i])) {
	                return true;
	            }
	        }
		return false;
	}
	
//test1
}
