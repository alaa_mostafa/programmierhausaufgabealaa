package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import controller.BunteRechteckeController;
import model.Rechteck;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class RechteckeLayoutmanagar extends JFrame {

	private JPanel contentPane;
	private JTextField tfdY;
	private JTextField tfdX;
	private JTextField tfdhoehe;
	private JTextField txtBreite;
	private final JLabel lblNewLabel = new JLabel("Rechteckwerte:");
	private BunteRechteckeController brc = new BunteRechteckeController();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RechteckeLayoutmanagar frame = new RechteckeLayoutmanagar();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RechteckeLayoutmanagar() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 567, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		panel.add(lblNewLabel);
		
		tfdX = new JTextField();
		tfdX.setText("X");
		panel.add(tfdX);
		tfdX.setColumns(10);
		
		tfdY = new JTextField();
		tfdY.setText("Y");
		panel.add(tfdY);
		tfdY.setColumns(10);
		
		tfdhoehe = new JTextField();
		tfdhoehe.setText("H�he");
		panel.add(tfdhoehe);
		tfdhoehe.setColumns(10);
		
		txtBreite = new JTextField();
		txtBreite.setText("Breite");
		panel.add(txtBreite);
		txtBreite.setColumns(10);
		
		JButton btnB = new JButton("Hinzuf\u00FCgen");
		btnB.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				
				if (e.getSource() == btnB) {
					int x = Integer.parseInt(tfdX.getText());
					int y = Integer.parseInt(tfdY.getText());
					int hoehe = Integer.parseInt(tfdhoehe.getText());
					int breite = Integer.parseInt(txtBreite.getText());
					
					Rechteck rechteck = new Rechteck(x, y, hoehe, breite);
					
					brc.add(rechteck);
					
							
				}
				
			}
		});
		contentPane.add(btnB, BorderLayout.SOUTH);
	}

}
